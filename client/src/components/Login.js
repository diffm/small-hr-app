import React, { useState, useRef } from 'react';
import { Button } from 'antd';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import CheckButton from 'react-validation/build/button';

import authService from '../services/auth.service';
import { modalGlobalConfig } from 'antd/lib/modal/confirm';

const required = (value) => {
    if (!value) {
        return (
            <div>
                This field is required.
            </div>
        )
    }
}

const Login = (props) => {
    const form = useRef();
    const checkBtn = useRef();

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [loading, setLoading] = useState(false);
    const [message, setMessage] = useState("");

    const onChangeEmail = (e) => {
        const email = e.target.value;
        setEmail(email);
    }

    const onChangePassword = (e) => {
        const password = e.target.value;
        setPassword(password);
    }

    const handleLogin = (e) => {
        e.preventDefault();

        setMessage("");
        setLoading(true);

        // form.current.validateAll();
        // console.log(checkBtn.current.context._errors)
        // if (checkBtn.current.context._errors.length===0) {
        //    console.log("123")
            authService.login(email, password)
                .then( (data) => {
                    props.history.push(`/${data.role}`);
                    window.location.reload();
                },
                (error) => {
                    const resMessage = (error.response && error.response.data && error.response.data.message)||
                                        error.message ||
                                        error.toString();
                    setLoading(false);
                    setMessage(resMessage);
                }) 
                // }else {
                //     setLoading(false);
        
    }

    return (
        <div>
            
            <form onSubmit={handleLogin} ref={form}>
                <div>
                    <label htmlFor="email">Email</label>
                    <input type="text" name="email" value={email} onChange={onChangeEmail} validations={[required]} />
                </div>
                <div>
                    <label htmlFor="password">Password</label>
                    <input type="password" name="password" value={password} onChange={onChangePassword} validations={[required]} />
                </div>
                <div>
                    <button type="primary" disabled={loading}>
                        {loading&&(
                            <span></span>
                        )}
                        <span>Login</span>
                    </button>
                </div>

                {message&&(
                    <div>
                        <div className="alert alert-danger" role="alert">
                            {message}
                        </div>
                    </div>
                )}
                {/* <CheckButton style={{display:"none"}} ref={checkBtn} /> */}
            </form>
            
        </div>
    )
}

export default Login;
