import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import EditUser from './EditUser';
import userService from '../../services/user.service';

const AdminBoard = ({message}) => {
    const [users, setUsers]=useState([]);
    const [localMessage, setLocalMessage] = useState("");
    const [prevPage, setPrevPage] = useState(null);
    const [nextPage, setNextPage] = useState(null);

    useEffect(()=>{
        fetchPage(1)
        
    },[]);

    const fetchPage = (idx) => {
        userService.getAdminBoard(idx).then(
            (response)=>{
                setUsers(response.data.records);
                setPrevPage(response.data.prev_page);
                setNextPage(response.data.next_page);
                setLocalMessage('');
            },
            (error)=>{
                setLocalMessage('Could not get the users!');
            }
        );
    }
    const handlePrevPage = () => {
      fetchPage(prevPage)
    }
    const handleNextPage = () => {
        fetchPage(nextPage)
    }

    const handleDelete = (e) => {
      const userId = parseInt(e.target.getAttribute('user_id'));

      userService.deleteUser(userId).then(() => {
        const otherUsers = users.filter(user=> user.id !== userId);
        setUsers(otherUsers)
      })
    }
    return (
        <div>
            {message && <div>{message}</div>}
            {localMessage && <div>{localMessage}</div>}
            {/* <header>
                {content}
            </header> */}
            <ul>
                {users.map((user) => (
                    
                    <li key={user.id}>
                        
                        <Link to={`/edituser/${user.id}`}>
                          <div>{user.username}</div>
                        </Link>
                        <button user_id={user.id} onClick={handleDelete}>Delete</button>
                    </li>
                ))}
            </ul>
            {prevPage && <button onClick={handlePrevPage}>Previous page</button>}
            {nextPage && <button onClick={handleNextPage}>Next page</button>}
        </div>
    )
}

export default AdminBoard;
