import React, { useState, useEffect } from 'react';
import userService from '../../services/user.service';

const UserBoard = () => {
    const [content, setContent]=useState("");

    useEffect(()=>{
        userService.getUserBoard().then(
            (response)=>{
                setContent(response.data);
            },
            (error)=>{
                const _content =
                (error.response&&
                    error.response.data&&
                    error.response.data.message)||
                    error.message||
                    error.toString();

                    setContent(_content);
            }
        );
    },[]);

    return (
        <div>
            <header>
                {content}
            </header>
        </div>
    )
}

export default UserBoard;
