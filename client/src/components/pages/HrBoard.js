import React, { useState, useEffect } from 'react';
import userService from '../../services/user.service';

import { Link } from 'react-router-dom';

const HrBoard = ({message}) => {
    const [employees, setEmployees]=useState([]);
    const [localMessage, setLocalMessage] = useState("");
    const [prevPage, setPrevPage] = useState(null);
    const [nextPage, setNextPage] = useState(null);

    useEffect(()=>{
        fetchPage(1)

    },[]);

    const fetchPage = (idx) => {
        userService.getEmployees(idx).then(
            (response)=>{
                setEmployees(response.data.records);
                setPrevPage(response.data.prev_page);
                setNextPage(response.data.next_page);
                setLocalMessage('');
            },
            (error)=>{
                setLocalMessage('Could not get the employees!');
            }
        );
    };
    const handlePrevPage = () => {
        fetchPage(prevPage)
      }
      const handleNextPage = () => {
          fetchPage(nextPage)
      }
    return (
        <div>
            {message && <div>{message}</div>}
            {localMessage && <div>{localMessage}</div>}

            <ul>
                {employees.map((employee) => (
                    
                    <li key={employee.id}>
                        
                        <Link to={`/edituser/${employee.id}`}>
                            <div>{employee.username}</div>
                        </Link>
                    </li>
                ))}
            </ul>
            {prevPage && <button onClick={handlePrevPage}>Previous page</button>}
            {nextPage && <button onClick={handleNextPage}>Next page</button>}

        </div>
    )
    }
    

export default HrBoard;
