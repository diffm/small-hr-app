import React, { useState, useRef, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import authService from '../../services/auth.service';
import userService from '../../services/user.service';
const EditUser = (props) => {
    let { id } = useParams();
    if (!id) {
      id =  props.id
    }
    console.log({id});
    const form = useRef();
    const checkBtn = useRef();

    const [username, setUsername]=useState("");
    const [email, setEmail]=useState("");
    const [role, setRole]=useState("");
    const [salary, setSalary]=useState("");
    const [number, setNumber]=useState("");
    // const [successful, setSuccessful]=useState(false);
    const [localMessage, setLocalMessage]=useState("");
    const [currentUser, setCurrentUser] = useState({})

    useEffect(() => {
        userService.getUser(id).then((response) => {
            const {user, profile} = response.data;
            setUsername(profile.username)
            setEmail(profile.email)
            setRole(user.role)
            setSalary(profile.salary)
            setNumber(profile.number) 
        })

        const usr = authService.getCurrentUser();
        setCurrentUser(usr)
    }, []);

    const onChangeUsername = (e) => {
        const username = e.target.value;
        setUsername(username);
    }

    const onChangeEmail = (e) => {
      const email = e.target.value;
      setEmail(email);
    }
    const onChangeRole = (e) => {
        const role = e.target.value;
        setRole(role);
    }
    const onChangeNumber = (e) => {
        const number = e.target.value;
        setNumber(number);
    }
    const onChangeSalary = (e) => {
        const salary = e.target.value;
        setSalary(salary);
    }

    const handleEditUser = (e) => {
        e.preventDefault();

        setLocalMessage("");
        // setSuccessful(false);

        // form.current.validateAll();

        // if (checkBtn.current.context._errors.length===0){
            userService.editUser(id, { username, email, role, salary, number})
                .then(
                    (response)=>{
                        alert(response.data.message);
                        
                        if (role  === 'admin' || role  === 'hr') {
                            // console.log(role)
                            props.history.push(`/${role}`);
                        } else {
                            setLocalMessage(response.data.message);
                        }
                    },
                    (error) => {
                        const resMessage = (error.response&&error.response.data&&error.response.data.message)
                                            || error.message
                                            || error.toString();
                        setLocalMessage(resMessage);
                    }
                )
        }

    return (
        <div>
            {localMessage&&(
                <div>
                    {localMessage}
                </div>
            )}
            <form onSubmit={handleEditUser} ref={form}>
               
                <div>
                <div>
                    <label htmlFor="username">Username</label>
                    <input type="text" name="username" value={username} onChange={onChangeUsername} />
                </div>
                <div>
                    <label htmlFor="email">Email</label>
                    <input type="email" name="email" value={email} onChange={onChangeEmail} />
                </div>
                {role && role === 'admin' && (
                    <div>
                        <label htmlFor="role">Role</label>
                        <input type="text" name="role" value={role} onChange={onChangeRole}  />
                    </div>
                ) }
                

           
                {((currentUser && currentUser.role === 'hr' && currentUser.id !== parseInt(id)) ||
                currentUser.role === 'admin') &&
                (
                    <div>
                        <label htmlFor="salary">Salary</label>
                        <input type="text" name="salary" value={salary} onChange={onChangeSalary}  />
                    </div>
                ) }
                <div>
                    <label htmlFor="number">Number</label>
                    <input type="text" name="number" value={number} onChange={onChangeNumber}  />
                </div>

                <div>
                    <button>Update</button>
                    </div>
                </div>
              

                {/* <CheckButton style={{display: 'none'}} ref={checkBtn} /> */}
            </form>
        </div>
    )
}

export default EditUser;
