import React, { useState, useRef } from 'react';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import CheckButton from 'react-validation/build/button';

import userService from '../services/user.service';

const required = (value) => {
    if (!value) {
        return (
            <div>
                This field is required.
            </div>
        )
    }
}

const validUsername = (value) => {
    if (value.length < 3 || value.length > 15) {
        return (
            <div>
                Username must be between 3 and 15 characters.
            </div>
        )
    }
}

const validPassword = (value) => {
    if (value.length < 6 || value.length > 12) {
        return (
            <div>
                Password must be between 6 and 12 characters.
            </div>
        )
    }
}

const CreateUser = (props) => {
    console.log("render")
    const form = useRef();
    const checkBtn = useRef();

    const [username, setUsername]=useState("");
    const [email, setEmail]=useState("");
    const [role, setRole]=useState("");
    const [salary, setSalary]=useState("");
    const [number, setNumber]=useState("");
    const [password, setPassword]=useState("");
    const [passwordConfirmation, setPasswordConfirmation]=useState("");
    const [successful, setSuccessful]=useState(false);
    const [localMessage, setLocalMessage]=useState("");

    const onChangeUsername = (e) => {
        const username = e.target.value;
        setUsername(username);
    }

    const onChangePassword = (e) => {
        const password = e.target.value;
        setPassword(password);
    }
    const onChangePasswordConfirmation = (e) => {
        const passwordConfirmation = e.target.value;
        setPasswordConfirmation(passwordConfirmation);
    }
    const onChangeEmail = (e) => {
      const email = e.target.value;
      setEmail(email);
    }
    const onChangeRole = (e) => {
        const role = e.target.value;
        setRole(role);
    }
    const onChangeNumber = (e) => {
        const number = e.target.value;
        setNumber(number);
    }
    const onChangeSalary = (e) => {
        const salary = e.target.value;
        setSalary(salary);
    }

    const handleCreateUser = (e) => {
        e.preventDefault();

        setLocalMessage("");
        setSuccessful(false);

        // form.current.validateAll();

        // if (checkBtn.current.context._errors.length===0){
            userService.createUser({username, email, role, salary, password, password_confirmantion: passwordConfirmation, number})
                .then(
                    (response)=>{
                        alert(response.data.message);
                        setSuccessful(true);
                        props.history.push('/admin')
                    },
                    (error) => {
                        const resMessage = (error.response&&error.response.data&&error.response.data.message)
                                            || error.message
                                            || error.toString();
                        setLocalMessage(resMessage);
                        setSuccessful(false);
                    }
                )
        }

    return (
        <div>
            {localMessage&&(
                <div>
                    {localMessage}
                </div>
            )}
            <form onSubmit={handleCreateUser} ref={form}>
                {!successful && (
                    <div>
                    <div>
                        <label htmlFor="username">Username</label>
                        <input type="text" name="username" value={username} onChange={onChangeUsername} validations={[required, validUsername]} />
                    </div>
                    {/* , role, salary, number */}
                    <div>
                        <label htmlFor="email">Email</label>
                        <input type="email" name="email" value={email} onChange={onChangeEmail} validations={[required, validUsername]} />
                    </div>
                    <div>
                        <label htmlFor="role">Role</label>
                        <input type="text" name="role" value={role} onChange={onChangeRole} validations={[required, validUsername]} />
                    </div>
                    <div>
                        <label htmlFor="salary">Salary</label>
                        <input type="text" name="salary" value={salary} onChange={onChangeSalary} validations={[required, validUsername]} />
                    </div>
                    <div>
                        <label htmlFor="number">Number</label>
                        <input type="text" name="number" value={number} onChange={onChangeNumber} validations={[required, validUsername]} />
                    </div>
                    <div>
                        <label htmlFor="password">Password</label>
                        <input type="password" name="password" value={password} onChange={onChangePassword} validations={[required, validPassword]} />
                    </div>
                    <div>
                        <label htmlFor="password_confirmation">Password confirmation</label>
                        <input type="password" name="password_confirmation" value={passwordConfirmation} onChange={onChangePasswordConfirmation} validations={[required, validPassword]} />
                    </div>
                    <div>
                        <button>Create User</button>
                        </div>
                    </div>
                )}

                {/* <CheckButton style={{display: 'none'}} ref={checkBtn} /> */}
            </form>
        </div>
    )
}

export default CreateUser;
