import React from 'react';
import authService from '../services/auth.service';

import EditUser from './pages/EditUser';

const Profile = (props) => {
    const currentUser = authService.getCurrentUser();

    return <EditUser id={currentUser.id} history={props.history}/>
}

export default Profile;
