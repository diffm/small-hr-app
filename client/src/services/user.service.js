import axios from 'axios';
import authHeader from './auth-header';

const API_URL = "http://localhost:3000/api/";

const createUser = (userAttributes) => {
    return axios.post(API_URL+"users", userAttributes, {headers:authHeader()});
};

const deleteUser = (id) => {
    return axios.delete(API_URL+`users/${id}`, {headers:authHeader()});
};
const editUser = (id, userAttributes) => {

    return axios.patch(API_URL+`users/${id}`, userAttributes, {headers: authHeader()});
};

const getPublicContent = () => {
    return axios.get(API_URL+"all");
};

const getUser = (id) => {
    return axios.get(API_URL+`users/${id}`, {headers:authHeader()});
};

const getEmployees = (pageIdx) => {
    return axios.get(API_URL+`users/employees?page=${pageIdx}`, {headers:authHeader()});
};

const getAdminBoard = (pageIdx) => {
    return axios.get(API_URL+`users/non_admin?page=${pageIdx}`, {headers:authHeader()});
};

export default {
    getPublicContent,
    getUser,
    getEmployees,
    getAdminBoard,
    createUser,
    deleteUser,
    editUser,
};
