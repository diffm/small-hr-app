import React, { useState, useEffect } from 'react';
import { Switch, Route, Link, useHistory} from 'react-router-dom';

import './App.css';

import authService from './services/auth.service';

import Login from './components/Login';
import CreateUser from './components/CreateUser';
import EditUser from './components/pages/EditUser';
import Home from './components/pages/Home';
import Profile from './components/Profile';
import HrBoard from './components/pages/HrBoard';
import AdminBoard from './components/pages/AdminBoard';

function App(props) {
  const [showHrBoard, setShowHrBoard] = useState(false);
  const [showAdminBoard, setShowAdminBoard] = useState(false);
  const [currentUser, setCurrentUser] = useState(null);
  const  [message, setMessage ] = useState("");

  let history = useHistory();
  
  useEffect(()=>{
    const user = authService.getCurrentUser();

    if (user) {
      setCurrentUser(user);
      setShowHrBoard(user.role.includes("hr"));
      setShowAdminBoard(user.role.includes("admin"));
    }
  },[])

  const handleLogout = () => {
    authService.logout();
    setCurrentUser(null);
    setShowHrBoard(false);
    setShowAdminBoard(false);
    history.push('/');
  }

  return (
    <div className="App">
      <nav>
        <Link to={'/'}>XYZ Company</Link>
        <div>
          <li>
            <Link to={'/home'}>
              Home
            </Link>
          </li>

          {showHrBoard&&(
            <li>
              <Link to={'/hr'}>
                Hr Board
              </Link>
              </li>
          )}

          {showAdminBoard&&(
            <ul>
            <li>
              <Link to={'/admin'}>
                Admin Board
              </Link>
              </li>
              <li>
            <Link to={'/createuser'}>
              Create User
            </Link>
                </li>
                </ul>
          )}
        </div>

        {currentUser ? (
          <div>
            <li>
              <Link to={'/profile'}>
                {currentUser.username}
              </Link>
            </li>
            <li>
              <button onClick={handleLogout}>
                LogOut
              </button>
            </li>
            </div>
        ) : (
          <div>
            <li>
              <Link to={'/login'}>
                Login
              </Link>
            </li>
            </div>
        )}
      </nav>

      <div>
        <Switch>
          <Route exact path={['/','/home']} component={Home} />
          <Route exact path='/login' component={Login} />
          <Route exact path='/createuser' component={CreateUser} />

          <Route exact path='/edituser/:id' component={EditUser} />
          <Route exact path='/profile' component={Profile} />
          <Route path='/hr' component={HrBoard} />
          <Route path='/admin' component={AdminBoard} />
        </Switch>
      </div>
    </div>
  );
}

export default App;
